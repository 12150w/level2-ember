# Configuration File
Each Level2 Ember project may contain a configuration file.
This file is optional and a project can be compiled without it.

## Creating the Configuration File
The configuration file is a JSON file that must be named `l2-config.json` and must be located at the root project folder.

## Standard Configuration Options
The following standard configuration options are provided to customize how your project is compiled and run.

### "app" Options (Optional)
The `app` attribute can define options passed to `Ember.Application.create` when starting your application.
For example this would set the root element of your application:

```json
{
    "app": {
        "rootElement": "#application-container"
    }
}
```

## Accessing Configuration
Apart from using the config file to change the standard configuration options any module may read the configuration file by loading the `config` module.
So, for example, you can define custom configuration options and access them in your project's code.
Or a plugin may define custom configuration options that can be controlled by your project's own config file.

For example: this shows how a custom configuration option can be read:

*l2-config.json*

```json
{
    "customOption": "custom value"
}
```

*some-module.js*

```javascript
var config = require('config');
console.log(config.customOption); // logs "custom value"
```
