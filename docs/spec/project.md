# Level2 Project Format
The Level2 Project Format is a file/folder structure that is used to describe Ember.js applications.
The Level2 Ember Compiler accepts applications in this format.

## The Base Folder
The **only** requirement to create a Level2 Ember project is an empty folder, everything else is optional.
When compiling you must provide the path to this folder in the [`Compiler` constructor](https://12150w.gitlab.io/level2-ember/api/classes/compiler.html#constructor).
An empty folder will compile into an empty Ember.js application rendered into the `<body>` tag.

!!!warning
    It is not recommended to use your project's root folder as the base folder.
    Using a specific folder just for the Level2 Project reduces the risk of file and folder naming conflicts.

## Modules
All javascript will run with Common JS modules available.
This means that a `require()` function, `exports` object, and `module.exports` object are available.

All modules within the base folder may be imported using a relative importe statement (starting with `.` or `..`).
**The module's file extension must be included** because the Level2 Ember Compiler uses the file extension to determine the file's exported type.

Objects defined on the `window` object may also be registered as global modules using the [Configuration File](config.md).
The Level2 Ember Compiler expects the following `window` properties are set and automatically registers them as global modules:

| Module Name    | Window Property           | Description                                   |
|----------------|---------------------------|-----------------------------------------------|
| `ember`        | `window.Ember`            | The Ember.js global object                    |
| `jquery`       | `window.jQuery`           | The jQuery instance to use with Ember.js      |
| `app`          | *not a window property*   | The `Ember.Application` instance              |

Here is an example of using modules.

```javascript
// Global modules are loaded using their name
const Ember = require('ember');

// ... and modules within the base folder are loaded using their relative from the current file
const CustomModule = require('./path/to/custom/module.js');

// Ember.js components need to have the file extension as well (they are treated the same way)
const SomeController = require('./path/to/some.controller');
```

## Ember.js Component Definition
Ember.js components are defined using the Common JS module specification.
The `module.exports` object must be set to the component definition (what is returned from `extend()`) if defining an Ember.js component -- otherwise you may export whatever you want.
The type of component defined in a file must match the type specified by the file's extension.
Here is an example of defining the index controller in a file named `index.controller`:

```javascript
// index.controller 
const { Controller } = require('ember');

// We set the module.exports object to our controller's definition.
module.exports = Controller.extend({
    // Put your code here
});
```

## Ember.js Component Resolution
Ember.js components are placed in the base folder or any sub-folder inside the base folder.
The name of the component is determined by the file's path and the type of the component is determined by the file's extension.
This means there is no need to have a folder named `templates`, `controllers`, `routes` and so on.
Each file may export only 1 component.

A component's name is determined by it's path relative to the base folder as follows:

* The path to the file and the filename without the extension are combined using forward slashes.
* If the filename **without the extension** and the name of the folder it is in are the same then we remove that last folder name:
* The file extension is added

Here are a few examples to show how this naming scheme works and its uses within an Ember.js application.
All the file paths are relatave to the project's base folder.

* Path: `application.controller`, Name: `application.controller`
    * Files in the base folder stay the same.
    * This is useful for global definitions, such as the application controller.
* Path: `admin/admin.controller`, Name: `admin.controller`
    * If the base name (the filename without the extension) is the same as the last folder name the last folder name is dropped.
    * This is handy when breaking up a project by functionality, each feature has a folder.
    * The same controller could be defined by a file named `admin.controller` in the base folder but it doesn't break up the feature entirely in its own folder.
* Path `admin/users.controller`, Name: `admin/users.controller`
    * The base name and the folder name are different so we keep the folder name.
    * This is useful for components inside of features that may not be large enough to require their own folder.
* Path `some/really/sub/feature/index.controller`, Name: `some/really/sub/feature/index.controller`
    * You can have any number of nested folders.
    * This is good for really large features.
* Path `admin/index.hbs`, Name: `admin/index`
    * Templates live in the same folder as everything else and follow the same naming conventions.

This naming method allows for finding component definitions from a top down approach, starting in the base project folder.
If a feature grows larger and it warrants its own folder moving the existing feature files to a folder of the same name does not change the name.

!!! warning
    If the same component is defined in two places, in the feature folder and outside, the compiler with throw an error.
    For example if both `admin.controller` and `admin/admin.controller` are defined the compoiler will report it as an error.

    The nice thing is that if you are looking for the admin controller you will have to go to the base folder first and look for a folder or file named "admin".
    If the admin controller is defined at this level you can look for it on your way to the admin folder. 

### Component & Helper Names
Because components and helpers may not have forward slashes in their name the Level2 Compiler replaces forward slashes with dashes.
So a component at `a/b/c/d.component` would be referenced in a template as `{{a-b-c-d}}`.
Here are some more examples to show how component and helper naming works:

* Path: `ui/input.component`, Name: `ui-input`
    * The forward slash in the path turns into a dash.
    * This is useful for namespacing components (really handy for libraries).
* Path `ui-input.component`, Name: `ui-input`
    * This is the same as the previous example, just to show the way to define the same component.
* Path `format/money.helper`, Name: `format-money`
    * Helper names follow the same rules as component names

Because of this special exceptions components can be defined at any level.
Try to keep components at as high a level as possible but you are free to make component names as long as you please.

When looking for a component make sure to start at the base folder and go down.
For example a component named `some-really-long-component` could be defined at:

* `some/really/long/component.component`
* `some/really/long-component.component`
* `some/really-long-component.component`
* `some-really-long-component.component`

If multiple definitions for the same component exist the compiler will report it as an error.

## Ember.js Component Types
These are the Ember.js component types that are supported and their corresponding file extensions.
Any thing unusual about how a component is referenced has been noted in the comments.

| Component Type      | File Extension          | Comments                                               |
|---------------------|-------------------------|--------------------------------------------------------|
| Controller          | `.controller`           |                                                        |
| Route               | `.route`                |                                                        |
| Template            | `.hbs`                  | Templates live in the same folder as everything else   |
| Component           | `.component`            | Components use dashes instead of `/` and `.`           |
| Helper              | `.helper`               | Helpers use dashes instead of `/` and `.`              |
| Initializer         | `.initializer`          |                                                        |
| *Anything Else*     | `.js`                   | Can be imported by another module                      |

!!! note
    You could use any file extension that you please (such as `.mixin` for Mixins) but these are the extensions that are *required* in order for the resolver to locate your components.
