# Level2 Ember Compiler

!!! warning
	This is a work in progress

The Level2 Ember Compiler is used to compile Ember.js applications in the Level2 Project format.
This is an experiment to see if there is an easier way to define Ember.js applications in a way that:

* Allows for project separation into folders per feature
* Allows for namespaced dependencies as if they were project components
* Prevents duplicate component definitions using the folder/file structure (as much as possible)
* Allows easy component lookup by using top down component lookups

This is the actual compiler that is used by various tools and plugins to produce compiled applications.
If just want to use the Level2 Ember Compiler, and not edit it, check out the [Tools and Plugins]() page.

## [API Documentation](api)
The API documentation is available [here](api)
