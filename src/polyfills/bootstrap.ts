/**
 * This script starts the ember application by reading the config and creating an ember application.
 */

moduleRegistry.define('app', function(require, module, exports) {
	let Ember = require('ember');
	let config = require('config');

	let Level2Resolver = Ember.DefaultResolver.extend({

		normalize(name) {
			return name.replace(/\./g, '/');
		},

		// resolveTemplate resolves a template.
		resolveTemplate(name) {
			let nameBase: string = Ember.String.dasherize(name.fullNameWithoutType);
			let nameParts = nameBase.replace(/\./g, '/').split('/');

			if(nameParts.length > 0 && nameParts[0] == 'components') {
				nameParts.splice(0, 1);
			}
			nameBase = nameParts.join('/');

			let componentName = `${nameBase}.component`;
			if(componentName in componentTemplateMap) {
				nameBase = componentTemplateMap[componentName];
			}
			
			return Ember.TEMPLATES[nameBase]
		},

		// resolveOther is the fallback to resolve a module.
		resolveOther(name): any {
			if(name.type == 'template') return;

			let nameBase: string = Ember.String.dasherize(name.fullNameWithoutType);
			let registryID = `${nameBase}.${name.type}`;
			if((name.type == 'component' || name.type == 'helper') && registryID in dashNameMap) {
				registryID = dashNameMap[registryID];
			}

			let cachedModule = moduleRegistry.getCached(registryID);
			if(cachedModule != undefined) {
				return cachedModule.exports;
			}
		}

	});

	let appConfig = config.app || {};
	appConfig.Resolver = Level2Resolver;

	module.exports = Ember.Application.create(appConfig);
});
