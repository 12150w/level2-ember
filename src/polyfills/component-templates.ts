
/** Maps component ids to the appropriate template ids that they should use. */
let componentTemplateMap: { [id: string]: string } = {};

/** Maps component and helper names to the appropriate module id to use. */
let dashNameMap: { [id: string]: string } = {};
