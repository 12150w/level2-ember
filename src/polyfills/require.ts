declare let L2_CONFIG: any;

/** Contains a listing of all defined require modules. */
class RequireRegistry {

    /** The absolute module ids that are supported. */
    private windowModules: { [id: string]: string } = {
        'ember': 'Ember',
        'jquery': 'jQuery'
    };

    /** The definded modules in this registry. */
    private cache: { [id: string]: RequireModule } = {};

    /** These are the definers that will create the modules. */
    private definers: { [id: string]: ModuleDefiner } = {};

    /** contains the pre-defined modules that are pending declaration. */
    private pendingModules: { [id: string]: PendingModule} = {};

    /** Returns true if the id is absolute */
    private static isAbsolute(id: string): boolean {
        if(id == null || id.length < 1) {
            return false;
        }
        if(id.substring(0, 1) == '.') {
            return false;
        }
        if(id.length > 1 && id.substring(0, 2) == '..') {
            return false;
        }

        return true;
    }

    /** Resolves a path relative to a module. */
    private static resolvePath(from: string, to: string): string {
        let resolved = from.split('/').slice(0, -1);

        // check for absolute require
        if(RequireRegistry.isAbsolute(to)) {
            return to;
        }
        
        // resolve relative require
        to.split('/').forEach(function(folder) {
            if(folder == '.') {
                return;
            } else if(folder == '..') {
                resolved.splice(resolved.length - 1, 1);
            } else {
                resolved.push(folder);
            }
        });

        return resolved.join('/');
    }

    /** Returns a require function relative to a given module path. */
    private buildRequire(path: string): RequireFunction {
        return (id: string) => {

            // handle special absolute modules
            if(RequireRegistry.isAbsolute(id)) {
                if(id == 'config') return L2_CONFIG;

                if(id in this.windowModules) {
                    let windowID = this.windowModules[id];
                    if(!(windowID in window)) {
                        throw new Error(`Unable to locate module "${id}": window.${windowID} is not defined`);
                    }
    
                    return window[windowID];
                }
            }
            let resolvedId = RequireRegistry.resolvePath(path, id);

            // handle cached modules
            if(resolvedId in this.cache) {
                return this.cache[resolvedId].exports;
            }

            // define the requested module
            if(!(resolvedId in this.definers)) {
                throw new Error(`Module not found "${resolvedId}"`);
            }
            return this.runDefiner(resolvedId, this.definers[resolvedId]).exports;
        }
    }

    /** 
     * Defines a module with the given id.
     * If the module is already defined an error is thrown.
     */
    define(id: string, factory: ModuleDefiner): void {
        if(id in this.definers) {
            throw new Error(
                `Module "${id}" defined multiple times` + '\n' +
                `---------- First Definition ----------` + '\n' +
                this.definers[id].defineStack + '\n' +
                `----------`
            )
        }

        factory.defineStack = (new Error()).stack;
        this.definers[id] = factory;
    }

    /**
     * Seal runs all the definers and adds the new modules to the registry.
     * After running the definers, throws an error if any requires are not defined.
     */
    seal(): void {
        for(let id in this.definers) {
            if(id in this.cache) continue;
            this.runDefiner(id, this.definers[id]);
        }
    }

    /** Runs a definer and adds the new module to the cache. */
    private runDefiner(id: string, definer: ModuleDefiner): RequireModule {
        let module: RequireModule = { exports: {} };
        this.cache[id] = module;

        let require = this.buildRequire(id);
        definer(require, module, module.exports);

        this.cache[id] = module;
        return module;
    }

    /** Returns the cached module if it is in the cache, or undefined if not. */
    getCached(id: string): RequireModule | undefined {
        return this.cache[id];
    }

}

/** The default registry to use for all modules. */
let moduleRegistry = new RequireRegistry();
window['TEST'] = moduleRegistry;

/** A module returned from a require call. */
interface RequireModule {

    /** The exported module contents. */
    exports: any;

}

/** A function that defines a module. */
interface ModuleDefiner {
    (require: RequireFunction, module: RequireModule, exports: any): void;

    /** The stack when this definer was added to the registry. */
    defineStack?: string;

}

/** A module that is waiting to be defined. */
class PendingModule {

    /** The id of the pending module. */
    readonly id: string;

    /** The call stack when the pending module was created. */
    readonly stack: string = (new Error).stack;

    /** Construct given the pending module's id. */
    constructor(id: string) {
        this.id = id;
    }

}

/** A require function is one that resolves dependencies. */
interface RequireFunction {
    (id: string): any;
}
