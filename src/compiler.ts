import { join as joinPath } from 'path';
import { TemplateCompiler } from './template-compiler';
import { Writable as WritableStream } from 'stream';
import { Module } from './module';
import { ContentWriter, WritableContent } from './content-writer';
import { StaticContent, PolyfillContent, StringContent, ConfigContent, TemplateContent } from './writers';
import { Registry } from './registry';
import * as Promise from 'bluebird';
import * as glob from 'glob';
import * as path from 'path';

/** The default ember template compiler file name. */
const defaultTemplateCompiler = 'ember-template-compiler.js';

/** A Compiler is used to compile a Level2 Ember application. */
export class Compiler {

    /** The path that this compiler will compile the project from */
    public readonly path: string;

    /** The template compiler instance this compiler will use. */
    readonly templateCompiler: TemplateCompiler;

    /** The compile time options to use. */
    readonly options: Options;

    /** The registry for modules. */
    readonly moduleRegistry = new Registry<Module>();

    /** The registry for modules with dashed names. */
    readonly dashedModuleRegistry = new Registry<Module>();

    /** The registry for templates. */
    readonly templateRegistry = new Registry<TemplateContent>();

    /** Construct given the path to the project and compiler's options. */
    constructor(path: string, options: Options) {
        this.path = path;
        this.options = options;
        let templateCompilerPath = options.templateCompiler || joinPath(this.path, defaultTemplateCompiler);
        this.templateCompiler = new TemplateCompiler(templateCompilerPath);
    }

    /** Compiles the contents of the project into the given writable stream. */
    compile(output: WritableStream): Promise<void> {
        return new Promise((resolve, reject) => {
            let writer = new ContentWriter(output);
            let hadError = false;

            writer.on('error', (err) => {
                if(hadError) return;
                hadError = true;
                reject(err);
            });

            writer.on('end', () => {
                if(hadError) return;
                resolve();
            });

            // beginning content
            return new Promise((resolve, reject) => {
                writer.add(new StringContent('(function() {\n'));
                writer.add(new ConfigContent(this.path));
                writer.add(new PolyfillContent('require.js'));
                writer.add(new PolyfillContent('component-templates.js'));
                resolve();
                
            }).then(() => {

                // application content
                return new Promise((resolve, reject) => {
                    glob('**/*.*', {cwd: this.path}, (err, files) => {
                        if(err != null) return reject(err);
        
                        for(let filePath of files) {
                            if(filePath == 'l2-config.json') continue;
                            let module = new Module(filePath, this);

                            writer.add(module.getContent());
                        }
        
                        resolve();
                    });
                });

            }).then(() => {

                // ending content
                writer.add(new PolyfillContent('bootstrap.js'));
                writer.add(new StringContent('\nmoduleRegistry.seal();\n'));
                writer.add(new StringContent('\n})();'));
                
            }).finally(() => writer.finish());
        });
    }

}

/** Options that can configure the compiler */
export interface Options {

    /** The path to the ember-template-compiler.js to override the default. */
    templateCompiler?: string

    /** If true then then the source is not in eval() functions. */
    disableDebug?: boolean

    /** An function that pre-processes script contents. */
    preprocessor?: { (code: string, id: string): string }

    /** If true then the sourceURL comment will not be added to definable content. */
    disableSourceURL?: boolean

}
