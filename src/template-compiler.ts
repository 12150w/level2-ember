import * as Promise from 'bluebird';

/** Allows compiling ember templates using an ember template compiler. */
export class TemplateCompiler {

    /** The path to the ember template compiler to use. */
    private path: string;

    /** The cached template compiler instance */
    private cachedCompiler: EmberTemplateCompiler;

    /** Construct given the path to the compiler to use. */
    constructor(path: string) {
        this.path = path;
    }

    /** Compiles a template returning its compiled contents. */
    compile(contents: string): Promise<string> {
        return this.getInstance().then((compiler) => {
            return compiler.precompile(contents, false);
        });
    }

    /** Gets the compiler instance, using the cached value if available. */
    private getInstance(): Promise<EmberTemplateCompiler> {
        if(this.cachedCompiler != null) {
            return Promise.resolve(this.cachedCompiler);
        }
        
        return new Promise((resolve, reject) => {
            try {
                let compiler: EmberTemplateCompiler = require(this.path);
                this.cachedCompiler = compiler;
                resolve(compiler);
            } catch(err) {
                if(err.code === 'MODULE_NOT_FOUND') {
                    reject(new Error(`Unable to find ember template compiler at ${this.path}`));
                }
                reject(err);
            }
        });
    }

}

/** Expected interface for an ember template compiler. */
declare interface EmberTemplateCompiler {

    /** Compiles the HTMLBars in template. */
    precompile(template: string, inline: boolean): string;

}
