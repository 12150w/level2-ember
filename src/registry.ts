
/** Registerable objects can be registered in a registry */
export interface Registerable {

	/** getID returns the id of the item which identifies it in the registry */
	getID(): string;

}

/** Keeps track of modules so that duplicate modules can be detected. */
export class Registry<T extends Registerable> {

	/** contains the registered types */
	private contents: Map<string, T>;

	/** construct with defaults */
	constructor() {
		this.contents = new Map();
	}

	/** adds an item to the registry, returning true if the item is not already registered. */
	register(item: T, itemID?: string): boolean {
		if(itemID == null) itemID = item.getID();

		if(this.contents.has(itemID)) {
			return false;
		}

		this.contents.set(itemID, item);
		return true;
	}

	/** Returns the item from the registry or undefined if the registry does not contain the item. */
	get(id: string): T {
		if(!this.contents.has(id)) throw new Error(`Registry does not contain id "${id}"`);
		return this.contents.get(id) as T;
	}

}
