import { FileContent } from './file-content';
import { WritableContent } from '../content-writer';
import { TemplateCompiler } from '../template-compiler';
import { DefinableContent } from './definable-content';
import { Registerable } from '../registry';
import { Compiler } from '../compiler';
import * as Promise from 'bluebird';
import * as path from 'path';

export class TemplateContent extends FileContent implements WritableContent, Registerable {

	/** The id to register the template with. */
	readonly id: string;

	/** The encoding to read the template with. */
	readonly encoding: string;

	/** The compiler to use to compile the template. */
	private compiler: Compiler;

	/** Construct given the template id and path to the template file. */
	constructor(id: string, filepath: string, compiler: Compiler, encoding = 'UTF-8') {
		super(filepath);
		this.compiler = compiler;
		this.encoding = encoding;

		let idData = path.parse(id);
		let idParts: string[];
		if(idData.dir.length > 0 && idData.dir != '/') {
			idParts = `${idData.dir}/${idData.name}`.split('/');
		} else {
			idParts = [idData.name];
		}
		if(idParts.length > 1 && idParts[idParts.length - 2] == idParts[idParts.length - 1]) {
			idParts.splice(idParts.length - 1, 1);
		}

		this.id = idParts.join('/');
	}

	/** Reads and compiles the template. */
	getContent(): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			if(this.compiler.templateRegistry.register(this)) {
				return resolve();
			}
			let existingTemplate = this.compiler.templateRegistry.get(this.getID()) as TemplateContent;
			reject(new Error(`Template "${this.id}" defined multiple times at ${this.path} and ${existingTemplate.path}`));

		}).then(() => this.loadFileContents()).then((templateBuffer) => {
			return this.compiler.templateCompiler.compile(templateBuffer.toString(this.encoding)).catch((err) => {
				throw new Error(`Unable to compile template ${this.path}:\n${err}`);
			});

		}).then((compiledTemplate) => {
			let escapedID = DefinableContent.escapeString(this.id);

			return new Buffer(
				`Ember.TEMPLATES['${escapedID}'] = Ember.Handlebars.template(${compiledTemplate});\n`,
				this.encoding
			);
		});
	}

	/** Returns the template id from the id. */
	getID(): string {
		return this.id;
	}

}
