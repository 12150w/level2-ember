import { WritableContent } from '../content-writer';
import { FileContent } from './file-content';
import * as Promise from 'bluebird'
import * as path from 'path';

/** Writable content that reads from the polyfills folder. */
export class PolyfillContent extends FileContent implements WritableContent {

    /** Construct given the path of a file relative to the polyfills folder. */
    constructor(polyfillPath: string) {
        super(path.join(__dirname, '..', 'polyfills', polyfillPath));
    }

    /** Loads the static content to write */
    getContent(): Promise<Buffer> {
        return this.loadFileContents().then((contents) => contents);
    }

}