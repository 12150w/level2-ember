import { FileContent } from './file-content';
import { WritableContent } from '../content-writer';
import * as Promise from 'bluebird';
import * as path from 'path';

/** Config content creates the configuration for the ember application. */
export class ConfigContent extends FileContent implements WritableContent {

	/** The encoding to write the configuration as. */
	readonly encoding: string;

	/** Construct given the base project path. */
	constructor(projectPath: string, encoding = 'UTF-8') {
		super(path.join(projectPath, 'l2-config.json'));
		this.encoding = encoding;
	}

	/** Loads the configuration into javascript. */
	getContent(): Promise<Buffer> {
		return this.loadFileContents().then((configBuffer) => {
			return JSON.parse(configBuffer.toString(this.encoding));

		}).catch((err) => {
			if(err.code !== 'ENOENT') throw err;
			return {};

		}).then((config) => {
			let configJSON = JSON.stringify(config);
			return new Buffer(`\nvar L2_CONFIG = ${configJSON};\n`, this.encoding);

		});
	}

}