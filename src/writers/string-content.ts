import { WritableContent } from '../content-writer';
import * as Promise from 'bluebird';

/** Writable content that writes a static string. */
export class StringContent implements WritableContent {

    /** The static data to write. */
    private data: Buffer;

    /** Construct given the string content to write. */
    constructor(content: string, encoding = 'UTF-8') {
        this.data = new Buffer(content, encoding);
    }

    /** Returns the static string content. */
    getContent(): Promise<Buffer> {
        return Promise.resolve(this.data);
    }

}
