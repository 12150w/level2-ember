import { WritableContent } from '../content-writer';
import { FileContent } from './file-content';
import * as Promise from 'bluebird'

/** Writable content that reads a file exactly as it is saved. */
export class StaticContent extends FileContent implements WritableContent {

    /** Loads the static content to write */
    getContent(): Promise<Buffer> {
        return this.loadFileContents().then((contents) => contents);
    }

}
