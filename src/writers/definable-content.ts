import { FileContent } from './file-content';
import { WritableContent } from '../content-writer';
import { Options } from '../compiler.js';
import * as Promise from 'bluebird';

/** Definable content is content that should be defined using the require() function. */
export class DefinableContent extends FileContent implements WritableContent {

    /** The encoding to load the define script with. */
    readonly encoding: string = 'UTF-8';

    /** The id to define the module content with. */
    readonly id: string;

    /** The optional content to append after the defined content. */
    private appendedContent: Buffer[] = [];

    /** The compile time options. */
    private options: Options;

    /** Construct given the id to define the content with. */
    constructor(id: string, path: string, options: Options) {
        super(path);
        this.id = id;
        this.options = options;
    }

    /** Escapes content to be placed in a javascript string. */
    static escapeString(input: string): string {
        if(input == null) return '';
        return input.replace(/'/g, "\\'").replace(/\r?\n/g, '\\n');
    }

    /** Loads the content and adds the require call to define the module. */
    getContent(): Promise<Buffer> {
        return this.loadFileContents().then((dataBuffer) => {
            let data = dataBuffer.toString(this.encoding);
            let escapedID = DefinableContent.escapeString(this.id);

            if(this.options.preprocessor != null) {
                data = this.options.preprocessor(data, this.id);
            }

            let sourceURL = '';
            if(this.options.disableSourceURL !== true) {
                sourceURL = `//# sourceURL=${this.id}`;
            }

            let sourceBuffer: Buffer;
            if(this.options.disableDebug) {
                sourceBuffer = new Buffer(`\n${data}\n`);
            } else {
                data = data.replace(/\\/g, '\\\\');
                sourceBuffer = new Buffer(`eval('${DefinableContent.escapeString(data)}\\n${sourceURL}'); `);
            }

            return Buffer.concat([
                new Buffer(`moduleRegistry.define('${escapedID}', function(require, module, exports) { `, this.encoding),
                sourceBuffer,
                new Buffer(`});\n`, this.encoding),
                ...this.appendedContent
            ]);
        });
    }

    /** Adds content to be placed after the default define content. */
    append(content: Buffer): DefinableContent {
        this.appendedContent.push(content);
        return this;
    }

}
