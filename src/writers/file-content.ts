import { WritableContent } from '../content-writer';
import * as Promise from 'bluebird';
import * as fs from 'fs';

/** Base class that represents content backed by an actual file. */
export abstract class FileContent {

    /** The path of the file that this content loads. */
    readonly path: string;
    
    /** Construct given the path to the file to load. */
    constructor(path: string) {
        this.path = path;
    }

    /** Loads the contents of the file for this content. */
    protected loadFileContents(): Promise<Buffer> {
        return new Promise((resolve, reject) => {
            fs.readFile(this.path, (err, data) => {
                if(err != null) return reject(err);
                resolve(data);
            });
        });
    }

}
