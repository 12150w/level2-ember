import { WritableContent } from './content-writer';
import { DefinableContent, TemplateContent } from './writers';
import { TemplateCompiler } from './template-compiler';
import { Registerable, Registry } from './registry';
import { Options, Compiler } from './compiler';
import * as path from 'path';

/** Module is a single part of an Ember application. */
export class Module implements Registerable {

    /** The type of the module */
    readonly type: ModuleType;

    /** The id of the module (defined by its path and type) */
    readonly id: string;

    /** The compiler to use for this module */
    private compiler: Compiler;

    /** The path to the module contents relative to the project it belongs in */
    readonly path: string;

    /** Construct given the raw name and base path of the module. */
    constructor(name: string, compiler: Compiler) {
        this.path = name;
        this.compiler = compiler;

        let data = path.parse(name.toLowerCase());
        let dirParts: string[] = [];
        if(data.dir.length > 0) {
            dirParts = data.dir.split(path.sep);
            if(dirParts[dirParts.length-1] == data.name.toLowerCase()) {
                dirParts.splice(dirParts.length-1, 1);
            }
        }
        let idParts = [...dirParts, data.name.toLowerCase()];
        this.id = idParts.join('/') + (data.ext || '').toLowerCase();
        
        switch(data.ext) {

            case '.controller':
                this.type = ModuleType.Controller;
                break;
            
            case '.route':
                this.type = ModuleType.Route;
                break;

            case '.component':
                this.type = ModuleType.Component;
                break;
            
            case '.helper':
                this.type = ModuleType.Helper;
                break;
            
            case '.hbs':
                this.type = ModuleType.Template;
                break;

            case '.js':
                this.type = ModuleType.General;
                break;

            default:
                this.type = ModuleType.Unknown;

        }
    }

    /** Returns the content for this module. */
    getContent(): WritableContent {
        switch(this.type) {

            case ModuleType.Controller:
                this.registerModule();
                return new DefinableContent(this.id, this.fullpath(), this.compiler.options);
            
            case ModuleType.Route:
                this.registerModule();
                return new DefinableContent(this.id, this.fullpath(), this.compiler.options);

            case ModuleType.Component:
                this.registerModule();
                this.registerDashedModule();
                let dashedID = DefinableContent.escapeString(this.getDashedID());
                let templateData = path.parse(this.id);
                let templateID = `${templateData.dir}/${templateData.name}`;

                return new DefinableContent(this.id, this.fullpath(), this.compiler.options).append(new Buffer(
                    `dashNameMap['${dashedID}'] = '${DefinableContent.escapeString(this.id)}';\n`,
                    'UTF-8'
                )).append(new Buffer(
                    `componentTemplateMap['${dashedID}'] = '${DefinableContent.escapeString(templateID)}';\n`,
                    'UTF-8'
                ));

            case ModuleType.Helper:
                this.registerModule();
                this.registerDashedModule();
                return new DefinableContent(this.id, this.fullpath(), this.compiler.options).append(new Buffer(
                    `dashNameMap['${DefinableContent.escapeString(this.getDashedID())}'] = '${DefinableContent.escapeString(this.id)}';\n`,
                    'UTF-8'
                ));
            
            case ModuleType.Template:
                this.registerModule();
                return new TemplateContent(this.id, this.fullpath(), this.compiler);

            case ModuleType.General:
                this.registerModule();
                return new DefinableContent(this.id, this.fullpath(), this.compiler.options);

            default:
                throw new Error(`Unable to get content for unsupported module type ${ModuleType[this.type]} at ${this.fullpath()}`);

        }
    }

    /** Returns the full path to the module's content */
    fullpath(): string {
        return path.join(this.compiler.path, this.path);
    }

    /** Returns the module's id. */
    getID(): string {
        let idData = path.parse(this.id);
        if(idData.dir == '/') return idData.base;
        let idParts = this.id.split('/');

        if(idParts.length > 1 && idParts[idParts.length - 2] == idData.name) {
            idParts.splice(idParts.length - 2, 1);
        }

        return idParts.join('/');
    }

    /** Returns the dasherized version of the module's id. */
    private getDashedID(): string {
        return this.getID().replace(/[\/]/g, '-').replace(/([A-Z])[^A-Z]/g, (match, letter) => {
            return letter.toLowerCase() + '-';
        });
    }

    /** Registers this module in the compiler, checking for duplicates. */
    private registerModule(): void {
        if(this.compiler.moduleRegistry.register(this)) return;
        let id = this.getID();
        let otherModule = this.compiler.moduleRegistry.get(id);

        throw new Error(`Module ${id} defined multiple times at ${this.path} and ${otherModule.path}`);
    }

    /** Registers this module with its dasherized name. */
    private registerDashedModule(): void {
        let id = this.getDashedID();

        if(this.compiler.dashedModuleRegistry.register(this, id)) return;
        let otherModule = this.compiler.dashedModuleRegistry.get(id);

        throw new Error(`Module ${id} defined multiple times at ${this.path} and ${otherModule.path}`);
    }

}

/** ModuleTypes are the types of modules supported by the compiler. */
export enum ModuleType {
    Controller,
    Route,
    Template,
    Helper,
    Component,
    General,
    Unknown
}
