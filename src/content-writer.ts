import { Writable } from 'stream';
import { EventEmitter } from 'events';
import * as Promise from 'bluebird';

/**
 * A Content Writer writes various content to a writable stream in sequential order.
 * The purpose of the this class is to make it easy to handle stream high water marks.
 */
export class ContentWriter extends EventEmitter {

    /** The number of milliseconds to wait between writing each content in the queue. */
    private static timeout = 0;

    /** The stream this writer is writing into. */
    private output: Writable;

    /** The queue of content to be written to the output. */
    private queue: WritableContent[] = [];

    /** When finished, and not more content is to be written, this is set to true. */
    private finished: boolean = false;

    /** 
     * Construct given the writable stream to write content into.
     * Once constructed the writer will be waiting for new content to write immediately.
     */
    constructor(output: Writable) {
        super();
        this.output = output;

        this.output.on('error', (err) => this.emit('error', err));

        this.process();
    }

    /** Adds some content to the queue to be written as soon as possible (first in first out). */
    add(content: WritableContent): void {
        if(this.finished) {
            throw new Error(`Unable to add content to content writer: this writer is finished`);
        }
        this.queue.push(content);
    }

    /**
     * Marks this content writer as finished and will prevent writing any more content to it.
     * Any content currently in the buffer will still be written before the `end` event is emitted.
     */
    finish(): void {
        this.finished = true;
    }

    /** Processes the next content in the queue and schedules itself to run again,
     * unless the writer is finished.
     */
    private process(): void {
        if(this.queue.length > 0) {
            let content = this.queue.shift() as WritableContent;

            content.getContent().then((data) => {
                return new Promise((resolve, reject) => {
                    this.output.write(data, () => this.process());
                });
            }).catch((err) => {
                this.emit('error', err);
            });
            
            return;
        }

        if(this.finished) {
            this.output.on('finish', () => {
                this.emit('end');
            });
            this.output.end();
            return;
        }
        setTimeout(() => {
            this.process();
        }, ContentWriter.timeout);
    }

}

/**
 * WritableContent is something that can be written by a content writer.
 */
export interface WritableContent {

    /** Gets the content to be written. */
    getContent(): Promise<Buffer>;

}
