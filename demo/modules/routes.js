const app = require('app');
app.Router.map(function() {
    this.route('test', function() {
        this.route('another');
        this.route('dash-dash');
        this.route('camelCase');
    });
});