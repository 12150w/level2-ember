const { Compiler } = require('../');
const path = require('path');
const fs = require('fs');

let compiler = new Compiler(path.join(__dirname, 'modules'), {
	templateCompiler: path.join(__dirname, '..', 'node_modules', 'ember-source', 'dist', 'ember-template-compiler.js')
});
let outFile = fs.createWriteStream(path.join(__dirname, 'app.js'));

compiler.compile(outFile).done();