/*
	Read Buffer

	The read buffer is used to test stream contents.
*/
const EventEmitter = require('events');

module.exports = class ReadBuffer extends EventEmitter {

	// construct given the stream to buffer
	constructor(stream) {
		super();
		this.data = Buffer.alloc(0);

		stream.on('data', (data) => this.data = Buffer.concat([this.data, data]));
		stream.on('end', () => this.emit('end'));
		stream.on('error', (err) => this.emit('error', err));
	}

}