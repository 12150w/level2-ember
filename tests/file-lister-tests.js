const { FileLister } = require('../');
const path = require('path');
const assert = require('assert');

describe('FileLister', () => {

	describe('#list', () => {

		it('lists the files matching a pattern', (done) => {
			let lister = new FileLister();
			let filesListed = {};
			
			lister.on('error', (err) => done(err));
			lister.on('file', (file) => filesListed[file] = true);
			lister.on('end', () => {
				assert(filesListed['a.txt'] === true);
				assert(filesListed['sub/a.txt'] === true);
				done();
			});

			lister.list('**/*.*', { cwd: path.join(__dirname, 'samples', 'list')});
			lister.finish();
		});

		it('passes the tag with the file listed', (done) => {
			let lister = new FileLister();
			let tagsListed = {};
			
			lister.on('error', (err) => done(err));
			lister.on('file', (file, tag) => tagsListed[file] = tag);
			lister.on('end', () => {
				assert(tagsListed['a.txt'] === 'base');
				assert(tagsListed['sub/a.txt'] === 'sub');
				done();
			});

			lister.list('*.*', { cwd: path.join(__dirname, 'samples', 'list')}, 'base');
			lister.list('sub/*.*', { cwd: path.join(__dirname, 'samples', 'list')}, 'sub');
			lister.finish();
		});

	});

});