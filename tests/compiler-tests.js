/*
	Compiler Tests

	Tests the Compiler.
*/
const { Compiler } = require('../');
const ReadBuffer = require('./read-buffer');
const path = require('path');
const COMPILER_PATH = path.join(__dirname, '..', 'node_modules', 'ember-source', 'dist', 'ember-template-compiler.js');

describe('Compiler', () => {
	it('adds require()', (done) => {
		let compiler = new Compiler(path.join(__dirname, 'samples', 'basic'), {
			templateCompiler: COMPILER_PATH
		});
		let buffer = new ReadBuffer(compiler);

		buffer.on('error', (err) => done(err));
		buffer.on('end', () => {
			done();
		});
	});
	it('adds module');
});